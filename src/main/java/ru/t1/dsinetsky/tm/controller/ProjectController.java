package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.IProjectController;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    private void showProject(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Desc: " + project.getDesc());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void create() {
        System.out.println("Enter name of new project:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        Project project = projectService.create(name, description);
        if (project == null) System.out.println("Error! Project cant be created");
        else System.out.println("Project successfully created!");
    }

    @Override
    public void show() {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        List<Project> listProjects = projectService.returnAll(sort);
        System.out.println("List of projects:");
        int index = 1;
        for (Project project : listProjects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Override
    public void clear() {
        projectService.clearAll();
        System.out.println("All projects successfully cleared!");
    }

    @Override
    public void showProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.findById(id);
        if (project == null)
            System.out.println("Project not found!");
        else
            showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project project = projectService.findByIndex(index);
        if (project == null)
            System.out.println("Project not found!");
        else
            showProject(project);
    }

    @Override
    public void updateProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.findById(id);
        if (project == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String description = TerminalUtil.nextLine();
            project = projectService.updateById(id, name, description);
            showProject(project);
            System.out.println("Project successfully updated!");
        }
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project project = projectService.findByIndex(index);
        if (project == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String description = TerminalUtil.nextLine();
            project = projectService.updateByIndex(index, name, description);
            showProject(project);
            System.out.println("Project successfully updated!");
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project project = projectTaskService.removeProjectById(id);
        if (project == null)
            System.out.println("Project not found!");
        else
            System.out.println("Project successfully removed!");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project project = projectTaskService.removeProjectByIndex(index);
        if (project == null)
            System.out.println("Project not found!");
        else
            System.out.println("Project successfully removed!");
    }

    @Override
    public void createTestProjects() {
        projectService.createTestProjects();
        System.out.println("10 test projects created!");
    }

    @Override
    public void startProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.changeStatusById(id, Status.IN_PROGRESS);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Project successfully started!");
        }
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project project = projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Project successfully started!");
        }
    }

    @Override
    public void completeProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.changeStatusById(id, Status.COMPLETED);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Project successfully completed!");
        }
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project project = projectService.changeStatusByIndex(index, Status.COMPLETED);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Project successfully completed!");
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Project project = projectService.changeStatusById(id, status);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Status successfully changed");
        }
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Project project = projectService.changeStatusByIndex(index, status);
        if (project == null)
            System.out.println("Project not found!");
        else {
            showProject(project);
            System.out.println("Status successfully changed");
        }
    }

}
