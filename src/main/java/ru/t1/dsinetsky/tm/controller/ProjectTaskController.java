package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.IProjectTaskController;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    private IProjectService projectService;

    private ITaskService taskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService, final IProjectService projectService, ITaskService taskService) {
        this.projectTaskService = projectTaskService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectEnter);
        if (project == null) {
            System.out.println("Project not found!");
            return;
        }
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskEnter);
        if (task == null) {
            System.out.println("Task not found!");
            return;
        }
        projectTaskService.bindProjectById(projectEnter, taskEnter);
        System.out.println("Task successfully bound to project!");
    }

    @Override
    public void unbindTaskToProjectById() {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectEnter);
        if (project == null) {
            System.out.println("Project not found!");
            return;
        }
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskEnter);
        if (task == null) {
            System.out.println("Task not found!");
            return;
        }
        projectTaskService.unbindProjectById(projectEnter, taskEnter);
        System.out.println("Task successfully unbound to project!");
    }

}
