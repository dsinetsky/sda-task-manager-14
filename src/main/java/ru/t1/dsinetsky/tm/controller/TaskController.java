package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.ITaskController;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDesc());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    private void showListTask(List<Task> listTasks) {
        System.out.println("List of tasks:");
        int i = 1;
        for (Task task : listTasks) {
            System.out.println(i + ". " + task.toString());
            i++;
        }
    }

    @Override
    public void showTasks() {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        showListTask(taskService.returnAll(sort));
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("Enter Id of project:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> listTasks = taskService.returnTasksOfProject(projectId);
        showListTask(listTasks);
    }

    @Override
    public void createTask() {
        System.out.println("Enter name of new task:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Task task = taskService.create(name, description);
        if (task == null) System.out.println("Error! Cant create new task");
        else System.out.println("Task successfully created!");
    }

    @Override
    public void clearTasks() {
        taskService.clearAll();
        System.out.println("Tasks successfully cleared!");
    }

    @Override
    public void showTaskById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null)
            System.out.println("Task not found!");
        else
            showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        Task task = taskService.findByIndex(index);
        if (task == null)
            System.out.println("Task not found!");
        else
            showTask(task);
    }

    @Override
    public void updateTaskById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null)
            System.out.println("Task not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String description = TerminalUtil.nextLine();
            task = taskService.updateById(id, name, description);
            showTask(task);
            System.out.println("Task successfully updated!");
        }
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        Task task = taskService.findByIndex(index);
        if (task == null)
            System.out.println("Task not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String description = TerminalUtil.nextLine();
            task = taskService.updateByIndex(index, name, description);
            showTask(task);
            System.out.println("Task successfully updated!");
        }
    }

    @Override
    public void removeTaskById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.removeById(id);
        if (task == null)
            System.out.println("Task not found!");
        else
            System.out.println("Task successfully removed!");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        Task task = taskService.removeByIndex(index);
        if (task == null)
            System.out.println("Task not found!");
        else
            System.out.println("Task successfully removed!");
    }

    @Override
    public void createTestTasks() {
        taskService.createTestTasks();
        System.out.println("10 test tasks created!");
    }

    @Override
    public void startTaskById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.changeStatusById(id, Status.IN_PROGRESS);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Task successfully started!");
        }
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        Task task = taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Task successfully started!");
        }
    }

    @Override
    public void completeTaskById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.changeStatusById(id, Status.COMPLETED);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Task successfully completed!");
        }
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        Task task = taskService.changeStatusByIndex(index, Status.COMPLETED);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Task successfully completed!");
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id of task:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Task task = taskService.changeStatusById(id, status);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Status successfully changed");
        }
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index of task:");
        int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Task task = taskService.changeStatusByIndex(index, status);
        if (task == null)
            System.out.println("Task not found!");
        else {
            showTask(task);
            System.out.println("Status successfully changed");
        }
    }

}
