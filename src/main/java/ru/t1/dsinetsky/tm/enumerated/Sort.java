package ru.t1.dsinetsky.tm.enumerated;

import ru.t1.dsinetsky.tm.comparator.CreatedComparator;
import ru.t1.dsinetsky.tm.comparator.NameComparator;
import ru.t1.dsinetsky.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value){
        if (value == null || value.isEmpty()) return null;
        for (Sort sort : values()){
            if (value.equals(sort.name())) return sort;
        }
        return null;
    }

    public static String getSortList(){
        StringBuilder sortList = new StringBuilder();
        for (Sort sort : values()){
            sortList.append(" - ").append(sort.name()).append("\n");
        }
        return sortList.toString();
    }

    public Comparator getComparator() {
        return comparator;
    }
}
