package ru.t1.dsinetsky.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static String getStatusList() {
        StringBuilder statusName = new StringBuilder();
        for (Status status : values())
            statusName.append(" - ").append(status.name()).append("\n");
        return statusName.toString();
    }

    public static Status toStatus(String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : values())
            if (value.equals(status.name())) return status;
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

}
