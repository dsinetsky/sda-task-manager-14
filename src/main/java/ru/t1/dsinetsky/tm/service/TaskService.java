package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void clearAll() {
        taskRepository.clearTasks();
    }

    @Override
    public List<Task> returnAll() {
        return taskRepository.returnAll();
    }

    @Override
    public List<Task> returnAll(Sort sort) {
        if (sort == null) return returnAll();
        return returnAll(sort.getComparator());
    }

    @Override
    public List<Task> returnAll(Comparator comparator) {
        if (comparator == null) return returnAll();
        return taskRepository.returnAll(comparator);
    }

    @Override
    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.createTask(new Task(name));
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.createTask(new Task(name, description));
    }

    @Override
    public Task findById(String id) {
        if (id != null && !id.isEmpty()) return taskRepository.findById(id);
        return null;
    }

    @Override
    public Task findByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id != null && !id.isEmpty()) return taskRepository.removeById(id);
        return null;
    }

    @Override
    public Task removeByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task updateByIndex(int index, String name, String description) {
        if (index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(int index, Status status) {
        if (index < 0) return null;
        if (index >= taskRepository.getSize()) return null;
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> returnTasksOfProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public void createTestTasks() {
        final Random randomizer = new Random();
        String name = "task";
        String description = "desc";
        for (int i = 1; i < 11; i++) {
            Task task = create(name + randomizer.nextInt(11), description + randomizer.nextInt(11));
            Status status;
            switch (randomizer.nextInt(3)){
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            task.setStatus(status);
        }
    }

}
