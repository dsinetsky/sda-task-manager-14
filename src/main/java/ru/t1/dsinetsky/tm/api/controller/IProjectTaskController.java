package ru.t1.dsinetsky.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskToProjectById();

}
