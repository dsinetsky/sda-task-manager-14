package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> returnAll();

    List<Project> returnAll(Comparator comparator);

    List<Project> returnAll(Sort sort);

    Project create(String name);

    Project create(String name, String desc);

    Project add(Project project);

    void clearAll();

    Project findById(String id);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByIndex(int index);

    Project updateById(String id, String name, String desc);

    Project updateByIndex(int index, String name, String desc);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(int index, Status status);

    void createTestProjects();

}
