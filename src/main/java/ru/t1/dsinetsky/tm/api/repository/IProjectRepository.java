package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> returnAll();

    List<Project> returnAll(Comparator comparator);

    Project add(Project project);

    void clearAll();

    Project findById(String id);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByIndex(int index);

    boolean existsById(String id);

    int getSize();
}
