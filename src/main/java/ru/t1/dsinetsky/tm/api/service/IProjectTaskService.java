package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectTaskService {

    void bindProjectById(String projectId, String taskId);

    void unbindProjectById(String projectId, String taskId);

    Project removeProjectById(String projectId);

    Project removeProjectByIndex(int index);

}
