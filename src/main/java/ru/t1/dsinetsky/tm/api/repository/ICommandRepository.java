package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
